﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Delers
// auteur: Kevin van Geemen
// datum: 5-7-2017
// functionaliteit: Bereken of input getal deelbaar is door input deler
// invoervelden: nummer, deler
// uitvoervelden: Textbox met ja of nee
// =================================================================================

namespace PO_Programmeren
{
    public partial class frm_delers : Form
    {
        public frm_delers()
        {
            InitializeComponent();
        }

        //Check button click
        private void btn_check_Click_1(object sender, EventArgs e)
        {
            //Kijken of nummer deelbaar is door deler
            if (nud_num.Value % nud_div.Value == 0)
            {
                //Zo ja, zet tekst in textbox
                rtb_out.Text = "Yes, " + Convert.ToString(nud_num.Value) + " is divisable by " + Convert.ToString(nud_div.Value) + ".";
            }
            else
            {
                //Zo nee, zet text in textbox
                rtb_out.Text = "No, " + Convert.ToString(nud_num.Value) + " is not divisable by " + Convert.ToString(nud_div.Value) + ".";
            }
        }
    }
}
