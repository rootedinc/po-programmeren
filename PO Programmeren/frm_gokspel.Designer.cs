﻿namespace PO_Programmeren
{
    partial class frm_gokspel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_schop = new System.Windows.Forms.Button();
            this.btn_hart = new System.Windows.Forms.Button();
            this.btn_klav = new System.Windows.Forms.Button();
            this.btn_ruit = new System.Windows.Forms.Button();
            this.pbx_image = new System.Windows.Forms.PictureBox();
            this.btn_guess = new System.Windows.Forms.Button();
            this.txt_guess = new System.Windows.Forms.TextBox();
            this.lbl_congrats = new System.Windows.Forms.Label();
            this.lbl_remaining = new System.Windows.Forms.Label();
            this.lbl_info = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_image)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_schop
            // 
            this.btn_schop.Location = new System.Drawing.Point(137, 226);
            this.btn_schop.Name = "btn_schop";
            this.btn_schop.Size = new System.Drawing.Size(75, 23);
            this.btn_schop.TabIndex = 0;
            this.btn_schop.Text = "Schoppen";
            this.btn_schop.UseVisualStyleBackColor = true;
            this.btn_schop.Click += new System.EventHandler(this.checkGuess);
            // 
            // btn_hart
            // 
            this.btn_hart.Location = new System.Drawing.Point(218, 226);
            this.btn_hart.Name = "btn_hart";
            this.btn_hart.Size = new System.Drawing.Size(75, 23);
            this.btn_hart.TabIndex = 1;
            this.btn_hart.Text = "Harten";
            this.btn_hart.UseVisualStyleBackColor = true;
            this.btn_hart.Click += new System.EventHandler(this.checkGuess);
            // 
            // btn_klav
            // 
            this.btn_klav.Location = new System.Drawing.Point(299, 226);
            this.btn_klav.Name = "btn_klav";
            this.btn_klav.Size = new System.Drawing.Size(75, 23);
            this.btn_klav.TabIndex = 2;
            this.btn_klav.Text = "Klaveren";
            this.btn_klav.UseVisualStyleBackColor = true;
            this.btn_klav.Click += new System.EventHandler(this.checkGuess);
            // 
            // btn_ruit
            // 
            this.btn_ruit.Location = new System.Drawing.Point(380, 226);
            this.btn_ruit.Name = "btn_ruit";
            this.btn_ruit.Size = new System.Drawing.Size(75, 23);
            this.btn_ruit.TabIndex = 3;
            this.btn_ruit.Text = "Ruiten";
            this.btn_ruit.UseVisualStyleBackColor = true;
            this.btn_ruit.Click += new System.EventHandler(this.checkGuess);
            // 
            // pbx_image
            // 
            this.pbx_image.Image = global::PO_Programmeren.Properties.Resources.spielkartenfarben;
            this.pbx_image.Location = new System.Drawing.Point(12, 12);
            this.pbx_image.Name = "pbx_image";
            this.pbx_image.Size = new System.Drawing.Size(559, 208);
            this.pbx_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_image.TabIndex = 4;
            this.pbx_image.TabStop = false;
            // 
            // btn_guess
            // 
            this.btn_guess.Location = new System.Drawing.Point(12, 158);
            this.btn_guess.Name = "btn_guess";
            this.btn_guess.Size = new System.Drawing.Size(109, 23);
            this.btn_guess.TabIndex = 5;
            this.btn_guess.Text = "Gok!";
            this.btn_guess.UseVisualStyleBackColor = true;
            this.btn_guess.Click += new System.EventHandler(this.btn_guess_Click);
            // 
            // txt_guess
            // 
            this.txt_guess.Location = new System.Drawing.Point(12, 132);
            this.txt_guess.Name = "txt_guess";
            this.txt_guess.Size = new System.Drawing.Size(559, 20);
            this.txt_guess.TabIndex = 6;
            // 
            // lbl_congrats
            // 
            this.lbl_congrats.AutoSize = true;
            this.lbl_congrats.Location = new System.Drawing.Point(12, 116);
            this.lbl_congrats.Name = "lbl_congrats";
            this.lbl_congrats.Size = new System.Drawing.Size(298, 13);
            this.lbl_congrats.TabIndex = 7;
            this.lbl_congrats.Text = "Gefeliciteerd, je hebt de kleur geraden! Nu nog de kaart zelf...";
            // 
            // lbl_remaining
            // 
            this.lbl_remaining.AutoSize = true;
            this.lbl_remaining.Location = new System.Drawing.Point(454, 116);
            this.lbl_remaining.Name = "lbl_remaining";
            this.lbl_remaining.Size = new System.Drawing.Size(117, 13);
            this.lbl_remaining.TabIndex = 8;
            this.lbl_remaining.Text = "Nog X pogingen over...";
            // 
            // lbl_info
            // 
            this.lbl_info.AutoSize = true;
            this.lbl_info.Location = new System.Drawing.Point(318, 163);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(253, 13);
            this.lbl_info.TabIndex = 9;
            this.lbl_info.Text = "Toegestane kaarten: A, K, Q, J en de nummers 2-10";
            // 
            // frm_gokspel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 261);
            this.Controls.Add(this.lbl_info);
            this.Controls.Add(this.lbl_remaining);
            this.Controls.Add(this.lbl_congrats);
            this.Controls.Add(this.txt_guess);
            this.Controls.Add(this.btn_guess);
            this.Controls.Add(this.pbx_image);
            this.Controls.Add(this.btn_ruit);
            this.Controls.Add(this.btn_klav);
            this.Controls.Add(this.btn_hart);
            this.Controls.Add(this.btn_schop);
            this.Name = "frm_gokspel";
            this.Text = "Gokspel";
            this.Load += new System.EventHandler(this.frm_gokspel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_schop;
        private System.Windows.Forms.Button btn_hart;
        private System.Windows.Forms.Button btn_klav;
        private System.Windows.Forms.Button btn_ruit;
        private System.Windows.Forms.PictureBox pbx_image;
        private System.Windows.Forms.Button btn_guess;
        private System.Windows.Forms.TextBox txt_guess;
        private System.Windows.Forms.Label lbl_congrats;
        private System.Windows.Forms.Label lbl_remaining;
        private System.Windows.Forms.Label lbl_info;
    }
}