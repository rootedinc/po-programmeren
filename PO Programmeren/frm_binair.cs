﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Binair
// auteur: H. Goor
// datum: 5-7-2017
// functionaliteit: Converteert een decimale, hexidecimale of binaire waarde naar de andere 2 waardes
// invoervelden: Decimale, hexidecimale of binaire waarde
// uitvoervelden: 2 Resterende waardes
// =================================================================================


namespace PO_Programmeren
{
    public partial class frm_binair : Form
    {
        int rb_checked = 0;

        public frm_binair()
        {
            InitializeComponent();
        }

        private void frm_binair_Load(object sender, EventArgs e)
        {
            TextBox[] toDisable = { txt_dec, txt_bin, txt_hex };
            foreach(TextBox x in toDisable)
            {
                x.ReadOnly = true;
            }
        }

        private void rb_dec_CheckedChanged(object sender, EventArgs e)
        {
            txt_dec.ReadOnly = false;
            txt_bin.ReadOnly = true;
            txt_hex.ReadOnly = true;

            rb_checked = 0;
        }

        private void rb_bin_CheckedChanged(object sender, EventArgs e)
        {
            txt_dec.ReadOnly = true;
            txt_bin.ReadOnly = false;
            txt_hex.ReadOnly = true;

            rb_checked = 1;
        }

        private void rb_hex_CheckedChanged(object sender, EventArgs e)
        {
            txt_dec.ReadOnly = true;
            txt_bin.ReadOnly = true;
            txt_hex.ReadOnly = false;

            rb_checked = 2;
        }

        private bool check_if_int(TextBox requested)
        {
            string to_check = requested.Text;

            int result;
            if(int.TryParse(to_check, out result))
            {
                if(result > 255 && requested.Name == "txt_dec")
                {
                    requested.Text = "255";
                }
                if(result < 0 && requested.Name == "txt_dec")
                {
                    requested.Text = "0";
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private void onTextChanged(object sender, EventArgs e)
        {
            if (!check_if_int((TextBox)sender) && !((TextBox)sender).ReadOnly && ((TextBox)sender).Name != "txt_hex")
            {
                ((TextBox)sender).Text = "";
                return;
            }

            switch(rb_checked)
            {
                case 0:
                    txt_bin.Text = dec_bin(double.Parse(txt_dec.Text));
                    txt_hex.Text = dec_hex(double.Parse(txt_dec.Text));
                    break;

                case 1:
                    txt_dec.Text = bin_dec(txt_bin.Text).ToString();
                    txt_hex.Text = dec_hex(bin_dec(txt_bin.Text));
                    break;

                case 2:
                    txt_dec.Text = hex_dec(txt_hex.Text).ToString();
                    txt_bin.Text = dec_bin(hex_dec(txt_hex.Text)).ToString();
                    break;
            }
        }

        private string dec_bin(double dec)
        {
            int exp = 0;
            while(Math.Pow(2, exp) < dec) { exp += 1; }

            Dictionary<int, int> powers = new Dictionary<int, int>();
            for(int i = exp; i >= 0; i--) { powers[i] = 0; }

            string res = "";
            while(dec > 0)
            {
                if(Math.Pow(2, exp) <= dec)
                {
                    dec -= Math.Pow(2, exp);
                    powers[exp] = 1;
                    exp -= 1;
                } else
                {
                    exp -= 1;
                }
            }

            foreach(int x in powers.Keys)
            {
                res += powers[x].ToString();
            }

            return res;
        }

        private string dec_hex(double dec)
        {
            int exp = 0;
            while (Math.Pow(16, exp) < dec) { exp += 1; }

            Dictionary<int, double> powers = new Dictionary<int, double>();
            for (int i = exp; i >= 0; i--) { powers[i] = 0; }

            string res = "";
            while (dec > 0)
            {
                if(dec < 16)
                {
                    powers[0] = dec;
                    dec -= dec;
                }

                if (Math.Pow(16, exp) <= dec)
                {
                    double rem = Math.Floor(dec / Math.Pow(16, exp));
                    dec -= Math.Pow(16, exp) * rem;
                    powers[exp] = rem;

                    if(exp > 0)
                        exp -= 1;
                }
                else
                {
                    if(exp > 0)
                        exp -= 1;
                }
            }

            foreach (int x in powers.Keys)
            {
                string to_be_added = powers[x].ToString();
                switch(powers[x].ToString())
                {
                    case "10":
                        to_be_added = "A";
                        break;

                    case "11":
                        to_be_added = "B";
                        break;

                    case "12":
                        to_be_added = "C";
                        break;

                    case "13":
                        to_be_added = "D";
                        break;

                    case "14":
                        to_be_added = "E";
                        break;

                    case "15":
                        to_be_added = "F";
                        break;
                }

                res += to_be_added;
            }

            return res;
        }

        private double bin_dec(string bin)
        {
            double res = 0;

            char[] charArray = bin.ToCharArray();
            Array.Reverse(charArray);
            string x = new string(charArray);

            for(int i = 0; i < bin.Length; i++)
            {
                if(x[i] == '1')
                {
                    res += Math.Pow(2, i);
                }
            }

            return res;
        }

        private double hex_dec(string bin)
        {
            double res = 0;

            char[] charArray = bin.ToCharArray();
            Array.Reverse(charArray);
            string x = new string(charArray);

            for (int i = 0; i < bin.Length; i++)
            {
                double add;
                double.TryParse(x[i].ToString(), out add);
                switch(x[i])
                {
                    case 'A':
                        add = 10;
                        break;
                    case 'B':
                        add = 11;
                        break;
                    case 'C':
                        add = 12;
                        break;
                    case 'D':
                        add = 13;
                        break;
                    case 'E':
                        add = 14;
                        break;
                    case 'F':
                        add = 15;
                        break;
                }

                res += add * Math.Pow(16, i);
            }

            return res;
        }
    }
}
