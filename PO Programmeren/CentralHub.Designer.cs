﻿namespace PO_Programmeren
{
    partial class frm_centralhub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ex1 = new System.Windows.Forms.Button();
            this.btn_ex2 = new System.Windows.Forms.Button();
            this.btn_ex3 = new System.Windows.Forms.Button();
            this.btn_ex4 = new System.Windows.Forms.Button();
            this.btn_ex5 = new System.Windows.Forms.Button();
            this.btn_ex6 = new System.Windows.Forms.Button();
            this.btn_ex_13 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_ex1
            // 
            this.btn_ex1.Location = new System.Drawing.Point(12, 8);
            this.btn_ex1.Name = "btn_ex1";
            this.btn_ex1.Size = new System.Drawing.Size(212, 23);
            this.btn_ex1.TabIndex = 0;
            this.btn_ex1.Text = "Breuken";
            this.btn_ex1.UseVisualStyleBackColor = true;
            this.btn_ex1.Click += new System.EventHandler(this.btn_ex1_Click);
            // 
            // btn_ex2
            // 
            this.btn_ex2.Location = new System.Drawing.Point(12, 37);
            this.btn_ex2.Name = "btn_ex2";
            this.btn_ex2.Size = new System.Drawing.Size(212, 23);
            this.btn_ex2.TabIndex = 1;
            this.btn_ex2.Text = "Binair";
            this.btn_ex2.UseVisualStyleBackColor = true;
            this.btn_ex2.Click += new System.EventHandler(this.btn_ex2_Click);
            // 
            // btn_ex3
            // 
            this.btn_ex3.Location = new System.Drawing.Point(12, 66);
            this.btn_ex3.Name = "btn_ex3";
            this.btn_ex3.Size = new System.Drawing.Size(212, 23);
            this.btn_ex3.TabIndex = 2;
            this.btn_ex3.Text = "Delers";
            this.btn_ex3.UseVisualStyleBackColor = true;
            this.btn_ex3.Click += new System.EventHandler(this.btn_ex3_Click);
            // 
            // btn_ex4
            // 
            this.btn_ex4.Location = new System.Drawing.Point(12, 95);
            this.btn_ex4.Name = "btn_ex4";
            this.btn_ex4.Size = new System.Drawing.Size(212, 23);
            this.btn_ex4.TabIndex = 3;
            this.btn_ex4.Text = "Mediaan";
            this.btn_ex4.UseVisualStyleBackColor = true;
            this.btn_ex4.Click += new System.EventHandler(this.btn_ex4_Click);
            // 
            // btn_ex5
            // 
            this.btn_ex5.Location = new System.Drawing.Point(12, 153);
            this.btn_ex5.Name = "btn_ex5";
            this.btn_ex5.Size = new System.Drawing.Size(212, 23);
            this.btn_ex5.TabIndex = 4;
            this.btn_ex5.Text = "Gokspel";
            this.btn_ex5.UseVisualStyleBackColor = true;
            this.btn_ex5.Click += new System.EventHandler(this.btn_ex5_Click);
            // 
            // btn_ex6
            // 
            this.btn_ex6.Location = new System.Drawing.Point(12, 124);
            this.btn_ex6.Name = "btn_ex6";
            this.btn_ex6.Size = new System.Drawing.Size(212, 23);
            this.btn_ex6.TabIndex = 5;
            this.btn_ex6.Text = "Oppervlaktes";
            this.btn_ex6.UseVisualStyleBackColor = true;
            this.btn_ex6.Click += new System.EventHandler(this.btn_ex6_Click);
            // 
            // btn_ex_13
            // 
            this.btn_ex_13.Location = new System.Drawing.Point(12, 182);
            this.btn_ex_13.Name = "btn_ex_13";
            this.btn_ex_13.Size = new System.Drawing.Size(212, 23);
            this.btn_ex_13.TabIndex = 6;
            this.btn_ex_13.Text = "Letters";
            this.btn_ex_13.UseVisualStyleBackColor = true;
            this.btn_ex_13.Click += new System.EventHandler(this.btn_ex_13_Click);
            // 
            // frm_centralhub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 216);
            this.Controls.Add(this.btn_ex_13);
            this.Controls.Add(this.btn_ex6);
            this.Controls.Add(this.btn_ex5);
            this.Controls.Add(this.btn_ex4);
            this.Controls.Add(this.btn_ex3);
            this.Controls.Add(this.btn_ex2);
            this.Controls.Add(this.btn_ex1);
            this.Name = "frm_centralhub";
            this.Text = "Central Hub";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_ex1;
        private System.Windows.Forms.Button btn_ex2;
        private System.Windows.Forms.Button btn_ex3;
        private System.Windows.Forms.Button btn_ex4;
        private System.Windows.Forms.Button btn_ex5;
        private System.Windows.Forms.Button btn_ex6;
        private System.Windows.Forms.Button btn_ex_13;
    }
}

