﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//=================================================================================
//programmanaam: inhoud en oppervlakte
//auteur: Kevin van Geemen
//datum: 5-7-2017
//functionaliteit: Bereken inhoud en oppervlakte van kubus, balk, bol en piramide
//invoervelden: a,b,c (straal, zijden, aangegeven in plaatje)
//uitvoervelden: Inhoud, Oppervlakte
//=================================================================================


namespace PO_Programmeren
{
    public partial class frm_oppervlakte : Form
    {
        public frm_oppervlakte()
        {
            InitializeComponent();

        }


        //Temporary variables
        double area = 0;
        double vol = 0;

        //Setting is welk type object is geselecteerd
        int setting = 0; //0 = cube, 1 = beam, 2 = ball, 3 = pyramid

        //Check button click
        private void btn_calc_Click(object sender, EventArgs e)
        {
            if(setting == 0) //Als kubus is gekozen
            {
                //Zet de variabelen om in inhoud en oppervlakte en zet die in textbox
                //Zie pseudocode voor de formules
                area = Math.Round(Math.Pow(Convert.ToDouble(nud_a.Value), 2) * 6,2);
                vol = Math.Round(Math.Pow(Convert.ToDouble(nud_a.Value), 3),2);
                tbx_area.Text = area.ToString();
                tbx_vol.Text = vol.ToString();
            }
            if (setting == 1)   //Als balk is gekozen
            {
                area = Math.Round(2 * Convert.ToDouble(nud_a.Value * nud_b.Value + nud_b.Value * nud_c.Value + nud_a.Value * nud_c.Value),2);
                vol = Math.Round(Convert.ToDouble(nud_a.Value*nud_b.Value*nud_c.Value),2);
                tbx_area.Text = area.ToString();
                tbx_vol.Text = vol.ToString();
            }
            if (setting == 2)   //Als bal is gekozen
            {
                area = Math.Round(4 * Math.PI * Math.Pow(Convert.ToDouble(nud_a.Value),2),2);
                vol = Math.Round(Math.Pow(Convert.ToDouble(nud_a.Value), 3) * Math.PI * 4 / 3,2);
                tbx_area.Text = area.ToString();
                tbx_vol.Text = vol.ToString();
            }
            if (setting == 3)   //Als piramide is gekozen
            {
                area = Math.Round(Math.Pow(Convert.ToDouble(nud_a.Value), 2) + 2 * (Convert.ToDouble(nud_a.Value) * Math.Sqrt(Math.Pow(.5 * Convert.ToDouble(nud_a.Value), 2) + Math.Pow(Convert.ToDouble(nud_b.Value), 2))),2);
                vol = Math.Round(Math.Pow(Convert.ToDouble(nud_a.Value), 2) * Convert.ToDouble(nud_b.Value) / 3,2);
                tbx_area.Text = area.ToString();
                tbx_vol.Text = vol.ToString();
            }
        }

        //Check clear button click
        private void btn_clear_Click(object sender, EventArgs e)
        {
            //Zet alle variabelen terug
            area = 0;
            vol = 0;
            tbx_area.Text = "";
            tbx_vol.Text = "";
            nud_a.Value = 0;
            nud_b.Value = 0;
            nud_c.Value = 0;
        }

        //Check exit button
        private void btn_exit_Click(object sender, EventArgs e)
        {
            //Eindig de applicatie
            Application.Exit();
        }

        //Check info button
        private void btn_info_Click(object sender, EventArgs e)
        {
            //Hier wordt een messagebox gecalled per setting
            if(setting == 0)
            {
                MessageBox.Show("Oppervlakte: a^2 * 6\nInhoud: a^3");
            }
            if (setting == 1)
            {
                MessageBox.Show("Oppervlakte: 2*(ab + bc + ac)\nInhoud: a*b*c");
            }
            if (setting == 2)
            {
                MessageBox.Show("Oppervlakte: 4 * pi * r^2\nInhoud: 4/3 * pi * r^3");
            }
            if (setting == 3)
            {
                MessageBox.Show("Oppervlakte: zijde^2 + 2(zijde*wortel( (.5*zijde)^2 + h^2 ) )\nInhoud: 1/3 * zijde^2 * h");
            }
        }

        //Het drukken van de knoppen
        //De setting wordt gezet,
        //de worden labels/numeric Up-Downs visible of invisible gemaakt,
        //het image wordt veranderd.
        private void btn_cube_Click(object sender, EventArgs e)
        {
            lbl_b.Visible = lbl_c.Visible = nud_b.Visible = nud_c.Visible = false;
            setting = 0;
            pbx_pic.Image = Properties.Resources.img_cube;
        }

        private void btn_beam_Click(object sender, EventArgs e)
        {
            lbl_b.Visible = lbl_c.Visible = nud_b.Visible = nud_c.Visible = true;
            setting = 1;
            pbx_pic.Image = Properties.Resources.img_beam;
        }

        private void btn_ball_Click(object sender, EventArgs e)
        {
            lbl_b.Visible = lbl_c.Visible = nud_b.Visible = nud_c.Visible = false;
            setting = 2;
            pbx_pic.Image = Properties.Resources.img_ball;
        }

        private void btn_pyramid_Click(object sender, EventArgs e)
        {
            lbl_b.Visible = nud_b.Visible = true;
            lbl_c.Visible = nud_c.Visible = false;
            setting = 3;
            pbx_pic.Image = Properties.Resources.img_pyramid;
        }
    }
}
