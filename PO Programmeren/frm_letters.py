from tkinter import *
from tkinter import ttk
import ctypes

def get_info(event):
    strWord=Word.get()
    if len(strWord)>0:
        strFirstLetter=strWord[0]
        strLastLetter=strWord[-1]
        lenWord=len(strWord)
        asciiFirstLetter=ord(strFirstLetter)
        asciiLastLetter=ord(strLastLetter)
    else:
        strFirstLetter="Error"
        strLastLetter="Error"
        lenWord="Error"
        asciiFirstLetter="Error"
        asciiLastLetter="Error"
        ctypes.windll.user32.MessageBoxW(0, "Invalid String Length, enter a word", "Error", 1)

    FirstLetter.delete(0, "end")
    FirstLetter.insert(0, strFirstLetter)

    LastLetter.delete(0, "end")
    LastLetter.insert(0, strLastLetter)

    Length.delete(0, "end")
    Length.insert(0, lenWord)

    AsciiValue1.delete(0, "end")
    AsciiValue1.insert(0, asciiFirstLetter)

    AsciiValue2.delete(0, "end")
    AsciiValue2.insert(0, asciiLastLetter)

root = Tk()

root.title("Opdracht 5 - Tekstfuncties")

Word = Entry(root)
Word.grid(row=0, column=0, pady=5, padx=20, sticky=W)

submitButton = Button(root, text="Submit")
submitButton.bind("<Button-1>", get_info)
submitButton.grid(row=1)

Label(root, text="First letter:").grid(row=2, column=0, sticky=W)
FirstLetter = Entry(root)
FirstLetter.grid(row=2, column=1, sticky=W)

Label(root, text="Last Letter:").grid(row=2, column=2, pady=5, sticky=W)
LastLetter = Entry(root)
LastLetter.grid(row=2, column=3, sticky=E)

Label(root, text="Ascii Value First Letter").grid(row=4, column=0, sticky=W)
AsciiValue1 = Entry(root)
AsciiValue1.grid(row=4, column=1, sticky=E)

Label(root, text="Ascii Value Last Letter").grid(row=4, column=2, pady=5, sticky=W)
AsciiValue2 = Entry(root)
AsciiValue2.grid(row=4, column=3, sticky=W)

Label(root, text="Length Word").grid(row=6, column=0, pady=5, sticky=W)
Length = Entry(root)
Length.grid(row=6, column=1, sticky=W)

root.mainloop()