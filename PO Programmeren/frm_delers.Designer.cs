﻿namespace PO_Programmeren
{
    partial class frm_delers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_check = new System.Windows.Forms.Button();
            this.rtb_out = new System.Windows.Forms.RichTextBox();
            this.nud_div = new System.Windows.Forms.NumericUpDown();
            this.nud_num = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nud_div)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_num)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Divider:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Number:";
            // 
            // btn_check
            // 
            this.btn_check.Location = new System.Drawing.Point(120, 100);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(114, 39);
            this.btn_check.TabIndex = 9;
            this.btn_check.Text = "Check";
            this.btn_check.UseVisualStyleBackColor = true;
            this.btn_check.Click += new System.EventHandler(this.btn_check_Click_1);
            // 
            // rtb_out
            // 
            this.rtb_out.Location = new System.Drawing.Point(28, 154);
            this.rtb_out.Name = "rtb_out";
            this.rtb_out.Size = new System.Drawing.Size(300, 91);
            this.rtb_out.TabIndex = 8;
            this.rtb_out.Text = "";
            // 
            // nud_div
            // 
            this.nud_div.Location = new System.Drawing.Point(208, 64);
            this.nud_div.Maximum = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.nud_div.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nud_div.Name = "nud_div";
            this.nud_div.Size = new System.Drawing.Size(120, 20);
            this.nud_div.TabIndex = 7;
            this.nud_div.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // nud_num
            // 
            this.nud_num.Location = new System.Drawing.Point(208, 23);
            this.nud_num.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nud_num.Name = "nud_num";
            this.nud_num.Size = new System.Drawing.Size(120, 20);
            this.nud_num.TabIndex = 6;
            // 
            // frm_delers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 268);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_check);
            this.Controls.Add(this.rtb_out);
            this.Controls.Add(this.nud_div);
            this.Controls.Add(this.nud_num);
            this.MaximizeBox = false;
            this.Name = "frm_delers";
            this.Text = "Delers";
            ((System.ComponentModel.ISupportInitialize)(this.nud_div)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_num)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.RichTextBox rtb_out;
        private System.Windows.Forms.NumericUpDown nud_div;
        private System.Windows.Forms.NumericUpDown nud_num;
    }
}