﻿using System;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Letters
// auteur: E. Boot
// datum: 5-7-2017
// functionaliteit: Eerste en laatste letter weergeven, en ascii code van beiden, en lengte woord
// invoervelden: 1 Woord
// uitvoervelden: Eerste letter, laatste letter, ascii code eerste letter, ascii code laatste letter, lengte woord
// =================================================================================

namespace PO_Programmeren
{
    public partial class frm_letters : Form
    {
        public frm_letters()
        {
            InitializeComponent();
        }

        private void txt_in_TextChanged(object sender, EventArgs e)
        {
            if(((TextBox)sender).Text.Length >= 1) // To keep it from erroring.
            {
                txt_first.Text = txt_in.Text[0].ToString(); // Get the first character using substring/
                txt_last.Text = txt_in.Text[txt_in.Text.Length - 1].ToString(); // Get the last one.

                txt_asc_1.Text = ((int)txt_in.Text[0]).ToString(); // Characters are stored as integers, so casting to an int shows the ASCII value.
                txt_asc_2.Text = ((int)txt_in.Text[txt_in.Text.Length - 1]).ToString(); // Same but for the last letter.

                txt_length.Text = txt_in.Text.Length.ToString(); // Display length of the text.
            }
        }
    }
}
