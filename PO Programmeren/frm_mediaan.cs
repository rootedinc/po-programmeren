﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Mediaan
// auteur: H. Goor
// datum: 5-7-2017
// functionaliteit: Vind de middelste waarde(n) van een lijst van getallen
// invoervelden: Lijst van getallen gescheiden door komma's
// uitvoervelden: De middelste waarde(n) van de lijst
// =================================================================================


namespace PO_Programmeren
{
    public partial class frm_mediaan : Form
    {
        public frm_mediaan()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            string input_arr_raw = "";
            foreach(char x in txt_ask.Text)
            {
                if(!(x == '{' || x == '}' || x == ' '))
                {
                    input_arr_raw += x;
                }
            }

            string[] input_arr = input_arr_raw.Split(',');
            List<double> double_arr = new List<double>();

            foreach (string x in input_arr)
            {
                double res;
                if(double.TryParse(x.Replace('.', ','), out res))
                {
                    double_arr.Add(res);
                }
            }

            if (double_arr.Count < 1)
            {
                txt_med.Text = "ERROR: Te weinig nummers!";
                return;
            }

            double_arr.Sort();
            if(double_arr.Count % 2 != 0)
            {
                txt_med.Text = double_arr[(double_arr.Count - 1) / 2].ToString();
            } else
            {
                txt_med.Text = ((double_arr[double_arr.Count / 2] + double_arr[double_arr.Count / 2 - 1]) / 2).ToString();
            }
        }
    }
}
