﻿namespace PO_Programmeren
{
    partial class frm_mediaan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_ask = new System.Windows.Forms.TextBox();
            this.lbl_ask = new System.Windows.Forms.Label();
            this.txt_med = new System.Windows.Forms.TextBox();
            this.lbl_mediaan = new System.Windows.Forms.Label();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_ask
            // 
            this.txt_ask.Location = new System.Drawing.Point(12, 25);
            this.txt_ask.Name = "txt_ask";
            this.txt_ask.Size = new System.Drawing.Size(260, 20);
            this.txt_ask.TabIndex = 0;
            // 
            // lbl_ask
            // 
            this.lbl_ask.AutoSize = true;
            this.lbl_ask.Location = new System.Drawing.Point(12, 9);
            this.lbl_ask.Name = "lbl_ask";
            this.lbl_ask.Size = new System.Drawing.Size(216, 13);
            this.lbl_ask.TabIndex = 1;
            this.lbl_ask.Text = "Lijst van nummers gescheiden met comma\'s:";
            // 
            // txt_med
            // 
            this.txt_med.Location = new System.Drawing.Point(69, 111);
            this.txt_med.Name = "txt_med";
            this.txt_med.Size = new System.Drawing.Size(203, 20);
            this.txt_med.TabIndex = 2;
            // 
            // lbl_mediaan
            // 
            this.lbl_mediaan.AutoSize = true;
            this.lbl_mediaan.Location = new System.Drawing.Point(12, 114);
            this.lbl_mediaan.Name = "lbl_mediaan";
            this.lbl_mediaan.Size = new System.Drawing.Size(51, 13);
            this.lbl_mediaan.TabIndex = 3;
            this.lbl_mediaan.Text = "Mediaan:";
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(12, 51);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(89, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Berekenen";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // frm_mediaan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 149);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.lbl_mediaan);
            this.Controls.Add(this.txt_med);
            this.Controls.Add(this.lbl_ask);
            this.Controls.Add(this.txt_ask);
            this.Name = "frm_mediaan";
            this.Text = "Mediaan Berekenen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_ask;
        private System.Windows.Forms.Label lbl_ask;
        private System.Windows.Forms.TextBox txt_med;
        private System.Windows.Forms.Label lbl_mediaan;
        private System.Windows.Forms.Button btn_calc;
    }
}