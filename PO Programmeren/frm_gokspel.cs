﻿using System;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Gokspel
// auteur: E. Boot
// datum: 5-7-2017
// functionaliteit: Gokspel waarbij de speler de kleur en waarde van de kaart moet raden
// invoervelden: Kleur kaart, waarde kaart
// uitvoervelden: Correct/incorrect
// =================================================================================


namespace PO_Programmeren
{
    public partial class frm_gokspel : Form
    {
        int colour;
        int card;
        int remaining;


        public frm_gokspel()
        {
            InitializeComponent();
            colour = new Random().Next(0, 4);
            card = new Random().Next(1, 14);
            remaining = 2;

            Console.WriteLine("Colour: " + colour.ToString() + "\nCard: " + card.ToString());
        }

        private void checkGuess(object sender, EventArgs e)
        {
            int guess = 0;
            switch(((Button)sender).Name)
            {
                case "btn_schop":
                    guess = 0;
                    break;

                case "btn_hart":
                    guess = 1;
                    break;

                case "btn_klav":
                    guess = 2;
                    break;

                case "btn_ruit":
                    guess = 3;
                    break;
            }

            if (guess == colour)
                nextStage();
            else
            {
                subtractRetry();
            }
        }

        private void subtractRetry(bool failmessage = true)
        {
            if (remaining > 1)
            {
                remaining -= 1;
                if(remaining == 1)
                {
                    lbl_remaining.Text = "Nog " + remaining.ToString() + " poging over...";
                } else
                {
                    lbl_remaining.Text = "Nog " + remaining.ToString() + " pogingen over...";
                }

                if(failmessage)
                    MessageBox.Show("Helaas, fout geraden, maar geef de moed niet op!", "Oeps!", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult res = MessageBox.Show("Geen kansen meer over!", "Helaas", MessageBoxButtons.RetryCancel);
                if (res == DialogResult.Retry)
                {
                    new frm_gokspel().Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Goed, dan vertrek ik!", "Muahahaha", MessageBoxButtons.OK);
                    this.Close();
                }
            }
        }

        private void nextStage()
        {
            btn_guess.Visible = true;
            txt_guess.Visible = true;
            lbl_congrats.Visible = true;
            lbl_remaining.Visible = true;
            lbl_info.Visible = true;

            pbx_image.Visible = false;
            btn_hart.Visible = false;
            btn_klav.Visible = false;
            btn_ruit.Visible = false;
            btn_schop.Visible = false;

            remaining = 5;
            lbl_remaining.Text = "Nog " + remaining.ToString() + " pogingen over...";
        }

        private void frm_gokspel_Load(object sender, EventArgs e)
        {
            btn_guess.Visible = false;
            txt_guess.Visible = false;
            lbl_congrats.Visible = false;
            lbl_remaining.Visible = false;
            lbl_info.Visible = false;
        }

        private void Win()
        {
            if(MessageBox.Show("Je hebt mij verslagen, gefeliciteerd! Opnieuw?", "Gefeliciteerd", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                new frm_gokspel().Show();
                this.Close();
            } else
            {
                this.Close();
            }
        }

        private void btn_guess_Click(object sender, EventArgs e)
        {
            int guess = 0;
            switch (txt_guess.Text)
            {
                case "A":
                case "a":
                    guess = 1;
                    break;

                case "J":
                case "j":
                    guess = 11;
                    break;

                case "Q":
                case "q":
                    guess = 12;
                    break;

                case "K":
                case "k":
                    guess = 13;
                    break;
            }

            if (guess != 0)
            {
                if (guess == card)
                    Win();
                else
                    subtractRetry();
            } else
            {
                if(int.TryParse(txt_guess.Text, out guess))
                {
                    if(guess >= 2 && guess <= 10)
                    {
                        if (guess == card)
                            Win();
                        else
                            subtractRetry();
                    }
                    else
                    {
                        subtractRetry(false);
                        MessageBox.Show("Ongeldige invoer, wel een poging eraf gehaald omdat je niet lezen kunt!");
                    }
                }
                else
                {
                    subtractRetry(false);
                    MessageBox.Show("Ongeldige invoer, wel een poging eraf gehaald omdat je niet lezen kunt!");
                }
            }
        }
    }
}
