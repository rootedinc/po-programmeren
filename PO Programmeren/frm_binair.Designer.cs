﻿namespace PO_Programmeren
{
    partial class frm_binair
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rb_dec = new System.Windows.Forms.RadioButton();
            this.rb_bin = new System.Windows.Forms.RadioButton();
            this.txt_bin = new System.Windows.Forms.TextBox();
            this.txt_dec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rb_hex = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_hex = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // rb_dec
            // 
            this.rb_dec.AutoSize = true;
            this.rb_dec.Location = new System.Drawing.Point(12, 12);
            this.rb_dec.Name = "rb_dec";
            this.rb_dec.Size = new System.Drawing.Size(113, 17);
            this.rb_dec.TabIndex = 0;
            this.rb_dec.TabStop = true;
            this.rb_dec.Text = "Decimal to the rest";
            this.rb_dec.UseVisualStyleBackColor = true;
            this.rb_dec.CheckedChanged += new System.EventHandler(this.rb_dec_CheckedChanged);
            // 
            // rb_bin
            // 
            this.rb_bin.AutoSize = true;
            this.rb_bin.Location = new System.Drawing.Point(12, 35);
            this.rb_bin.Name = "rb_bin";
            this.rb_bin.Size = new System.Drawing.Size(104, 17);
            this.rb_bin.TabIndex = 1;
            this.rb_bin.TabStop = true;
            this.rb_bin.Text = "Binary to the rest";
            this.rb_bin.UseVisualStyleBackColor = true;
            this.rb_bin.CheckedChanged += new System.EventHandler(this.rb_bin_CheckedChanged);
            // 
            // txt_bin
            // 
            this.txt_bin.Location = new System.Drawing.Point(255, 136);
            this.txt_bin.MaxLength = 8;
            this.txt_bin.Name = "txt_bin";
            this.txt_bin.Size = new System.Drawing.Size(100, 20);
            this.txt_bin.TabIndex = 2;
            this.txt_bin.TextChanged += new System.EventHandler(this.onTextChanged);
            // 
            // txt_dec
            // 
            this.txt_dec.Location = new System.Drawing.Point(255, 107);
            this.txt_dec.MaxLength = 3;
            this.txt_dec.Name = "txt_dec";
            this.txt_dec.Size = new System.Drawing.Size(100, 20);
            this.txt_dec.TabIndex = 3;
            this.txt_dec.TextChanged += new System.EventHandler(this.onTextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Decimal Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Binary Number:";
            // 
            // rb_hex
            // 
            this.rb_hex.AutoSize = true;
            this.rb_hex.Location = new System.Drawing.Point(12, 58);
            this.rb_hex.Name = "rb_hex";
            this.rb_hex.Size = new System.Drawing.Size(136, 17);
            this.rb_hex.TabIndex = 6;
            this.rb_hex.TabStop = true;
            this.rb_hex.Text = "Hexadecimal to the rest";
            this.rb_hex.UseVisualStyleBackColor = true;
            this.rb_hex.CheckedChanged += new System.EventHandler(this.rb_hex_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Hexadecimal number:";
            // 
            // txt_hex
            // 
            this.txt_hex.Location = new System.Drawing.Point(255, 162);
            this.txt_hex.MaxLength = 2;
            this.txt_hex.Name = "txt_hex";
            this.txt_hex.Size = new System.Drawing.Size(100, 20);
            this.txt_hex.TabIndex = 11;
            this.txt_hex.TextChanged += new System.EventHandler(this.onTextChanged);
            // 
            // frm_binair
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 206);
            this.Controls.Add(this.txt_hex);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rb_hex);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_dec);
            this.Controls.Add(this.txt_bin);
            this.Controls.Add(this.rb_bin);
            this.Controls.Add(this.rb_dec);
            this.Name = "frm_binair";
            this.Text = "Binair";
            this.Load += new System.EventHandler(this.frm_binair_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rb_dec;
        private System.Windows.Forms.RadioButton rb_bin;
        private System.Windows.Forms.TextBox txt_bin;
        private System.Windows.Forms.TextBox txt_dec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rb_hex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_hex;
    }
}