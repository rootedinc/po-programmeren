﻿using System;
using System.Windows.Forms;

// =================================================================================
// programmanaam: Breuken
// auteur: H. Goor
// datum: 5-7-2017
// functionaliteit: 2 Breuken optellen, aftrekken, vermenigvuldigen of delen
// invoervelden: 2 Tellers, 2 noemers,
// uitvoervelden: Teller, noemer
// =================================================================================

namespace PO_Programmeren
{
    public partial class frm_breuken : Form
    {
        public frm_breuken()
        {
            InitializeComponent();
        }

        private double gcd(double a, double b) // Function to find the largest common divider.
        {
            int remainder;
            while( b != 0)
            {
                remainder = (int)(a % b);
                a = b;
                b = remainder;
            }

            return a;
        }

        private void txt_oper_TextChanged(object sender, EventArgs e)
        {
            switch(txt_oper.Text) // To make sure nothing else but these characters are entered.
            {
                case "+":
                    break;
                case "=":
                    txt_oper.Text = "+";
                    break;
                case "-":
                    break;
                case "/":
                    break;
                case ":":
                    break;
                case "*":
                    break;
                case "8":
                    txt_oper.Text = "*";
                    break;
                default:
                    txt_oper.Text = ""; // Resets textbox's text when other character is entered.
                    break;
            }
        }

        private void check_text_event(object sender, EventArgs e)
        {
            if(sender is TextBox)
            {
                TextBox txt = (TextBox)sender;
                if(!check_int(txt.Text))
                {
                    txt.Text = ""; // When the text is not a number, reset the textbox's text.
                }
            }
        }

        private bool check_all() // Basically the same as check_text_event but for all the textboxes.
        {
            TextBox[] boxes = { txt_div1_1, txt_div1_2, txt_div2_1, txt_div2_2, txt_oper };
            foreach(TextBox x in boxes)
            {
                if(x.Name == "txt_oper")
                {
                    if(string.IsNullOrEmpty(x.Text))
                    {
                        txt_res_1.Text = "";
                        txt_res_2.Text = "";
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                double number;
                if (!double.TryParse(x.Text, out number) || string.IsNullOrEmpty(x.Text))
                {
                    txt_res_1.Text = "";
                    txt_res_2.Text = "";
                    return false;
                }
            }

            return true;
        }

        private void collect_event(object sender, EventArgs e) // Event which combines two other events.
        {
            if(sender is TextBox)
            {
                if(((TextBox)sender).Name == "txt_oper") // txt_oper has other allowed characters.
                {
                    txt_oper_TextChanged(sender, e);
                }
                else
                {
                    check_text_event(sender, e);
                }
            }
            update_result(check_all());
        }

        private void update_result(bool success)
        {
            if(success)
            {
                // Initialise two empty variables for later use.
                double tmp1 = 0;
                double tmp2 = 0;
                switch(txt_oper.Text) // Actually do the math.
                {
                    case "+":
                        tmp1 = double.Parse(txt_div1_1.Text) * double.Parse(txt_div2_2.Text) + double.Parse(txt_div2_1.Text) * double.Parse(txt_div1_2.Text);
                        tmp2 = double.Parse(txt_div1_2.Text) * double.Parse(txt_div2_2.Text);
                        break;

                    case "-":
                        tmp1 = double.Parse(txt_div1_1.Text) * double.Parse(txt_div2_2.Text) - double.Parse(txt_div2_1.Text) * double.Parse(txt_div1_2.Text);
                        tmp2 = double.Parse(txt_div1_2.Text) * double.Parse(txt_div2_2.Text);
                        break;

                    case "*":
                        tmp1 = double.Parse(txt_div1_1.Text) * double.Parse(txt_div2_1.Text);
                        tmp2 = double.Parse(txt_div1_2.Text) * double.Parse(txt_div2_2.Text);
                        break;

                    case "/":
                        tmp1 = double.Parse(txt_div1_1.Text) * double.Parse(txt_div2_2.Text);
                        tmp2 = double.Parse(txt_div1_2.Text) * double.Parse(txt_div2_1.Text);
                        break;
                }


                double g = gcd(tmp1, tmp2); // Find the greatest common divider for our temporary numbers.

                // Make the remaining fraction as small as possible.
                txt_res_1.Text = (tmp1 / g).ToString();
                txt_res_2.Text = (tmp2 / g).ToString();
            }
        }

        private bool check_int(string to_test)
        {
            // Returns true when the string is a number, else it returns false.

            double number;
            if(double.TryParse(to_test, out number))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
