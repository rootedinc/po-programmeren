﻿namespace PO_Programmeren
{
    partial class frm_letters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_in = new System.Windows.Forms.TextBox();
            this.txt_first = new System.Windows.Forms.TextBox();
            this.txt_asc_1 = new System.Windows.Forms.TextBox();
            this.txt_length = new System.Windows.Forms.TextBox();
            this.txt_last = new System.Windows.Forms.TextBox();
            this.txt_asc_2 = new System.Windows.Forms.TextBox();
            this.lbl_in = new System.Windows.Forms.Label();
            this.lbl_first = new System.Windows.Forms.Label();
            this.lbl_asc_1 = new System.Windows.Forms.Label();
            this.lbl_length = new System.Windows.Forms.Label();
            this.lbl_last = new System.Windows.Forms.Label();
            this.lbl_asc_2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_in
            // 
            this.txt_in.Location = new System.Drawing.Point(12, 25);
            this.txt_in.Name = "txt_in";
            this.txt_in.Size = new System.Drawing.Size(546, 20);
            this.txt_in.TabIndex = 0;
            this.txt_in.TextChanged += new System.EventHandler(this.txt_in_TextChanged);
            // 
            // txt_first
            // 
            this.txt_first.Location = new System.Drawing.Point(130, 133);
            this.txt_first.Name = "txt_first";
            this.txt_first.Size = new System.Drawing.Size(100, 20);
            this.txt_first.TabIndex = 1;
            // 
            // txt_asc_1
            // 
            this.txt_asc_1.Location = new System.Drawing.Point(130, 159);
            this.txt_asc_1.Name = "txt_asc_1";
            this.txt_asc_1.Size = new System.Drawing.Size(100, 20);
            this.txt_asc_1.TabIndex = 2;
            // 
            // txt_length
            // 
            this.txt_length.Location = new System.Drawing.Point(130, 185);
            this.txt_length.Name = "txt_length";
            this.txt_length.Size = new System.Drawing.Size(100, 20);
            this.txt_length.TabIndex = 3;
            // 
            // txt_last
            // 
            this.txt_last.Location = new System.Drawing.Point(458, 133);
            this.txt_last.Name = "txt_last";
            this.txt_last.Size = new System.Drawing.Size(100, 20);
            this.txt_last.TabIndex = 4;
            // 
            // txt_asc_2
            // 
            this.txt_asc_2.Location = new System.Drawing.Point(458, 159);
            this.txt_asc_2.Name = "txt_asc_2";
            this.txt_asc_2.Size = new System.Drawing.Size(100, 20);
            this.txt_asc_2.TabIndex = 5;
            // 
            // lbl_in
            // 
            this.lbl_in.AutoSize = true;
            this.lbl_in.Location = new System.Drawing.Point(9, 9);
            this.lbl_in.Name = "lbl_in";
            this.lbl_in.Size = new System.Drawing.Size(117, 13);
            this.lbl_in.TabIndex = 6;
            this.lbl_in.Text = "Please enter some text:";
            // 
            // lbl_first
            // 
            this.lbl_first.AutoSize = true;
            this.lbl_first.Location = new System.Drawing.Point(12, 136);
            this.lbl_first.Name = "lbl_first";
            this.lbl_first.Size = new System.Drawing.Size(66, 13);
            this.lbl_first.TabIndex = 7;
            this.lbl_first.Text = "Eerste letter:";
            // 
            // lbl_asc_1
            // 
            this.lbl_asc_1.AutoSize = true;
            this.lbl_asc_1.Location = new System.Drawing.Point(12, 162);
            this.lbl_asc_1.Name = "lbl_asc_1";
            this.lbl_asc_1.Size = new System.Drawing.Size(116, 13);
            this.lbl_asc_1.TabIndex = 8;
            this.lbl_asc_1.Text = "ASCII van eerste letter:";
            // 
            // lbl_length
            // 
            this.lbl_length.AutoSize = true;
            this.lbl_length.Location = new System.Drawing.Point(12, 188);
            this.lbl_length.Name = "lbl_length";
            this.lbl_length.Size = new System.Drawing.Size(99, 13);
            this.lbl_length.TabIndex = 9;
            this.lbl_length.Text = "Lengte van de text:";
            // 
            // lbl_last
            // 
            this.lbl_last.AutoSize = true;
            this.lbl_last.Location = new System.Drawing.Point(381, 136);
            this.lbl_last.Name = "lbl_last";
            this.lbl_last.Size = new System.Drawing.Size(71, 13);
            this.lbl_last.TabIndex = 10;
            this.lbl_last.Text = "Laatste letter:";
            // 
            // lbl_asc_2
            // 
            this.lbl_asc_2.AutoSize = true;
            this.lbl_asc_2.Location = new System.Drawing.Point(319, 162);
            this.lbl_asc_2.Name = "lbl_asc_2";
            this.lbl_asc_2.Size = new System.Drawing.Size(133, 13);
            this.lbl_asc_2.TabIndex = 11;
            this.lbl_asc_2.Text = "ASCII van de laatste letter:";
            // 
            // frm_letters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 261);
            this.Controls.Add(this.lbl_asc_2);
            this.Controls.Add(this.lbl_last);
            this.Controls.Add(this.lbl_length);
            this.Controls.Add(this.lbl_asc_1);
            this.Controls.Add(this.lbl_first);
            this.Controls.Add(this.lbl_in);
            this.Controls.Add(this.txt_asc_2);
            this.Controls.Add(this.txt_last);
            this.Controls.Add(this.txt_length);
            this.Controls.Add(this.txt_asc_1);
            this.Controls.Add(this.txt_first);
            this.Controls.Add(this.txt_in);
            this.Name = "frm_letters";
            this.Text = "Letter Magic";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_in;
        private System.Windows.Forms.TextBox txt_first;
        private System.Windows.Forms.TextBox txt_asc_1;
        private System.Windows.Forms.TextBox txt_length;
        private System.Windows.Forms.TextBox txt_last;
        private System.Windows.Forms.TextBox txt_asc_2;
        private System.Windows.Forms.Label lbl_in;
        private System.Windows.Forms.Label lbl_first;
        private System.Windows.Forms.Label lbl_asc_1;
        private System.Windows.Forms.Label lbl_length;
        private System.Windows.Forms.Label lbl_last;
        private System.Windows.Forms.Label lbl_asc_2;
    }
}