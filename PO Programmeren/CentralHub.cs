﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PO_Programmeren
{
    public partial class frm_centralhub : Form
    {
        public frm_centralhub()
        {
            InitializeComponent();
        }

        private void btn_ex1_Click(object sender, EventArgs e)
        {
            new frm_breuken().Show();
        }

        private void btn_ex2_Click(object sender, EventArgs e)
        {
            new frm_binair().Show();
        }

        private void btn_ex3_Click(object sender, EventArgs e)
        {
            new frm_delers().Show();
        }

        private void btn_ex4_Click(object sender, EventArgs e)
        {
            new frm_mediaan().Show();
        }

        private void btn_ex6_Click(object sender, EventArgs e)
        {
            new frm_oppervlakte().Show();
        }

        private void btn_ex5_Click(object sender, EventArgs e)
        {
            new frm_gokspel().Show();
        }

        private void btn_ex_13_Click(object sender, EventArgs e)
        {
            new frm_letters().Show();
        }
    }
}
