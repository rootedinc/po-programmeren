﻿namespace PO_Programmeren
{
    partial class frm_oppervlakte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbx_pic = new System.Windows.Forms.PictureBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_area = new System.Windows.Forms.Label();
            this.lbl_volume2 = new System.Windows.Forms.Label();
            this.lbl_area2 = new System.Windows.Forms.Label();
            this.tbx_vol = new System.Windows.Forms.TextBox();
            this.tbx_area = new System.Windows.Forms.TextBox();
            this.lbl_vol = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nud_c = new System.Windows.Forms.NumericUpDown();
            this.lbl_c = new System.Windows.Forms.Label();
            this.nud_b = new System.Windows.Forms.NumericUpDown();
            this.lbl_b = new System.Windows.Forms.Label();
            this.nud_a = new System.Windows.Forms.NumericUpDown();
            this.lbl_a = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_pyramid = new System.Windows.Forms.Button();
            this.btn_ball = new System.Windows.Forms.Button();
            this.btn_beam = new System.Windows.Forms.Button();
            this.btn_cube = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_info = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pic)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_c)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_b)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_a)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbx_pic);
            this.groupBox1.Controls.Add(this.btn_clear);
            this.groupBox1.Controls.Add(this.btn_calc);
            this.groupBox1.Controls.Add(this.lbl_area);
            this.groupBox1.Controls.Add(this.lbl_volume2);
            this.groupBox1.Controls.Add(this.lbl_area2);
            this.groupBox1.Controls.Add(this.tbx_vol);
            this.groupBox1.Controls.Add(this.tbx_area);
            this.groupBox1.Controls.Add(this.lbl_vol);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(578, 297);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pbx_pic
            // 
            this.pbx_pic.Image = global::PO_Programmeren.Properties.Resources.img_cube;
            this.pbx_pic.Location = new System.Drawing.Point(306, 19);
            this.pbx_pic.Name = "pbx_pic";
            this.pbx_pic.Size = new System.Drawing.Size(256, 256);
            this.pbx_pic.TabIndex = 10;
            this.pbx_pic.TabStop = false;
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(104, 263);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 9;
            this.btn_clear.Text = "Wis";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(15, 263);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 8;
            this.btn_calc.Text = "Bereken";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_area
            // 
            this.lbl_area.AutoSize = true;
            this.lbl_area.Location = new System.Drawing.Point(12, 209);
            this.lbl_area.Name = "lbl_area";
            this.lbl_area.Size = new System.Drawing.Size(93, 13);
            this.lbl_area.TabIndex = 7;
            this.lbl_area.Text = "De oppervlakte is:";
            // 
            // lbl_volume2
            // 
            this.lbl_volume2.AutoSize = true;
            this.lbl_volume2.Location = new System.Drawing.Point(265, 235);
            this.lbl_volume2.Name = "lbl_volume2";
            this.lbl_volume2.Size = new System.Drawing.Size(24, 13);
            this.lbl_volume2.TabIndex = 6;
            this.lbl_volume2.Text = "cm²";
            // 
            // lbl_area2
            // 
            this.lbl_area2.AutoSize = true;
            this.lbl_area2.Location = new System.Drawing.Point(265, 209);
            this.lbl_area2.Name = "lbl_area2";
            this.lbl_area2.Size = new System.Drawing.Size(24, 13);
            this.lbl_area2.TabIndex = 5;
            this.lbl_area2.Text = "cm²";
            // 
            // tbx_vol
            // 
            this.tbx_vol.Location = new System.Drawing.Point(189, 232);
            this.tbx_vol.Name = "tbx_vol";
            this.tbx_vol.Size = new System.Drawing.Size(70, 20);
            this.tbx_vol.TabIndex = 4;
            // 
            // tbx_area
            // 
            this.tbx_area.Location = new System.Drawing.Point(189, 206);
            this.tbx_area.Name = "tbx_area";
            this.tbx_area.Size = new System.Drawing.Size(70, 20);
            this.tbx_area.TabIndex = 3;
            // 
            // lbl_vol
            // 
            this.lbl_vol.AutoSize = true;
            this.lbl_vol.Location = new System.Drawing.Point(12, 235);
            this.lbl_vol.Name = "lbl_vol";
            this.lbl_vol.Size = new System.Drawing.Size(69, 13);
            this.lbl_vol.TabIndex = 2;
            this.lbl_vol.Text = "De inhoud is:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nud_c);
            this.groupBox3.Controls.Add(this.lbl_c);
            this.groupBox3.Controls.Add(this.nud_b);
            this.groupBox3.Controls.Add(this.lbl_b);
            this.groupBox3.Controls.Add(this.nud_a);
            this.groupBox3.Controls.Add(this.lbl_a);
            this.groupBox3.Location = new System.Drawing.Point(6, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(289, 99);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Invoer";
            // 
            // nud_c
            // 
            this.nud_c.Location = new System.Drawing.Point(163, 71);
            this.nud_c.Name = "nud_c";
            this.nud_c.Size = new System.Drawing.Size(120, 20);
            this.nud_c.TabIndex = 5;
            this.nud_c.Visible = false;
            // 
            // lbl_c
            // 
            this.lbl_c.AutoSize = true;
            this.lbl_c.Location = new System.Drawing.Point(6, 73);
            this.lbl_c.Name = "lbl_c";
            this.lbl_c.Size = new System.Drawing.Size(141, 13);
            this.lbl_c.TabIndex = 4;
            this.lbl_c.Text = "Geef de afmeting van c (cm)";
            this.lbl_c.Visible = false;
            // 
            // nud_b
            // 
            this.nud_b.Location = new System.Drawing.Point(163, 46);
            this.nud_b.Name = "nud_b";
            this.nud_b.Size = new System.Drawing.Size(120, 20);
            this.nud_b.TabIndex = 3;
            this.nud_b.Visible = false;
            // 
            // lbl_b
            // 
            this.lbl_b.AutoSize = true;
            this.lbl_b.Location = new System.Drawing.Point(6, 47);
            this.lbl_b.Name = "lbl_b";
            this.lbl_b.Size = new System.Drawing.Size(141, 13);
            this.lbl_b.TabIndex = 2;
            this.lbl_b.Text = "Geef de afmeting van b (cm)";
            this.lbl_b.Visible = false;
            // 
            // nud_a
            // 
            this.nud_a.Location = new System.Drawing.Point(163, 19);
            this.nud_a.Name = "nud_a";
            this.nud_a.Size = new System.Drawing.Size(120, 20);
            this.nud_a.TabIndex = 1;
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(6, 21);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(141, 13);
            this.lbl_a.TabIndex = 0;
            this.lbl_a.Text = "Geef de afmeting van a (cm)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_pyramid);
            this.groupBox2.Controls.Add(this.btn_ball);
            this.groupBox2.Controls.Add(this.btn_beam);
            this.groupBox2.Controls.Add(this.btn_cube);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 74);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kies welk figuur je wilt:";
            // 
            // btn_pyramid
            // 
            this.btn_pyramid.Location = new System.Drawing.Point(98, 45);
            this.btn_pyramid.Name = "btn_pyramid";
            this.btn_pyramid.Size = new System.Drawing.Size(75, 23);
            this.btn_pyramid.TabIndex = 3;
            this.btn_pyramid.Text = "Piramide";
            this.btn_pyramid.UseVisualStyleBackColor = true;
            this.btn_pyramid.Click += new System.EventHandler(this.btn_pyramid_Click);
            // 
            // btn_ball
            // 
            this.btn_ball.Location = new System.Drawing.Point(9, 45);
            this.btn_ball.Name = "btn_ball";
            this.btn_ball.Size = new System.Drawing.Size(75, 23);
            this.btn_ball.TabIndex = 2;
            this.btn_ball.Text = "Bol";
            this.btn_ball.UseVisualStyleBackColor = true;
            this.btn_ball.Click += new System.EventHandler(this.btn_ball_Click);
            // 
            // btn_beam
            // 
            this.btn_beam.Location = new System.Drawing.Point(98, 19);
            this.btn_beam.Name = "btn_beam";
            this.btn_beam.Size = new System.Drawing.Size(75, 23);
            this.btn_beam.TabIndex = 1;
            this.btn_beam.Text = "Balk";
            this.btn_beam.UseVisualStyleBackColor = true;
            this.btn_beam.Click += new System.EventHandler(this.btn_beam_Click);
            // 
            // btn_cube
            // 
            this.btn_cube.Location = new System.Drawing.Point(9, 19);
            this.btn_cube.Name = "btn_cube";
            this.btn_cube.Size = new System.Drawing.Size(75, 23);
            this.btn_cube.TabIndex = 0;
            this.btn_cube.Text = "Kubus";
            this.btn_cube.UseVisualStyleBackColor = true;
            this.btn_cube.Click += new System.EventHandler(this.btn_cube_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(515, 315);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 1;
            this.btn_exit.Text = "Sluit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_info
            // 
            this.btn_info.Location = new System.Drawing.Point(434, 315);
            this.btn_info.Name = "btn_info";
            this.btn_info.Size = new System.Drawing.Size(75, 23);
            this.btn_info.TabIndex = 2;
            this.btn_info.Text = "Info";
            this.btn_info.UseVisualStyleBackColor = true;
            this.btn_info.Click += new System.EventHandler(this.btn_info_Click);
            // 
            // frm_oppervlakte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 350);
            this.Controls.Add(this.btn_info);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "frm_oppervlakte";
            this.Text = "Oppervlakte en Inhoud";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pic)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_c)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_b)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_a)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_volume2;
        private System.Windows.Forms.Label lbl_area2;
        private System.Windows.Forms.TextBox tbx_vol;
        private System.Windows.Forms.TextBox tbx_area;
        private System.Windows.Forms.Label lbl_vol;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown nud_c;
        private System.Windows.Forms.Label lbl_c;
        private System.Windows.Forms.NumericUpDown nud_b;
        private System.Windows.Forms.Label lbl_b;
        private System.Windows.Forms.NumericUpDown nud_a;
        private System.Windows.Forms.Label lbl_a;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_area;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_info;
        private System.Windows.Forms.PictureBox pbx_pic;
        private System.Windows.Forms.Button btn_pyramid;
        private System.Windows.Forms.Button btn_ball;
        private System.Windows.Forms.Button btn_beam;
        private System.Windows.Forms.Button btn_cube;
    }
}