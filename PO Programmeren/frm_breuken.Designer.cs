﻿namespace PO_Programmeren
{
    partial class frm_breuken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_div1_1 = new System.Windows.Forms.TextBox();
            this.txt_div1_2 = new System.Windows.Forms.TextBox();
            this.lbl_div1_div = new System.Windows.Forms.Label();
            this.txt_oper = new System.Windows.Forms.TextBox();
            this.lbl_div2_div = new System.Windows.Forms.Label();
            this.txt_div2_2 = new System.Windows.Forms.TextBox();
            this.txt_div2_1 = new System.Windows.Forms.TextBox();
            this.lbl_equals = new System.Windows.Forms.Label();
            this.lbl_res_div = new System.Windows.Forms.Label();
            this.txt_res_2 = new System.Windows.Forms.TextBox();
            this.txt_res_1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_div1_1
            // 
            this.txt_div1_1.Location = new System.Drawing.Point(16, 12);
            this.txt_div1_1.Name = "txt_div1_1";
            this.txt_div1_1.Size = new System.Drawing.Size(98, 20);
            this.txt_div1_1.TabIndex = 0;
            this.txt_div1_1.TextChanged += new System.EventHandler(this.collect_event);
            // 
            // txt_div1_2
            // 
            this.txt_div1_2.Location = new System.Drawing.Point(16, 51);
            this.txt_div1_2.Name = "txt_div1_2";
            this.txt_div1_2.Size = new System.Drawing.Size(98, 20);
            this.txt_div1_2.TabIndex = 1;
            this.txt_div1_2.TextChanged += new System.EventHandler(this.collect_event);
            // 
            // lbl_div1_div
            // 
            this.lbl_div1_div.AutoSize = true;
            this.lbl_div1_div.Location = new System.Drawing.Point(17, 35);
            this.lbl_div1_div.Name = "lbl_div1_div";
            this.lbl_div1_div.Size = new System.Drawing.Size(97, 13);
            this.lbl_div1_div.TabIndex = 2;
            this.lbl_div1_div.Text = "------------------------------";
            // 
            // txt_oper
            // 
            this.txt_oper.Location = new System.Drawing.Point(133, 33);
            this.txt_oper.MaxLength = 1;
            this.txt_oper.Name = "txt_oper";
            this.txt_oper.Size = new System.Drawing.Size(25, 20);
            this.txt_oper.TabIndex = 3;
            this.txt_oper.TextChanged += new System.EventHandler(this.collect_event);
            // 
            // lbl_div2_div
            // 
            this.lbl_div2_div.AutoSize = true;
            this.lbl_div2_div.Location = new System.Drawing.Point(176, 35);
            this.lbl_div2_div.Name = "lbl_div2_div";
            this.lbl_div2_div.Size = new System.Drawing.Size(97, 13);
            this.lbl_div2_div.TabIndex = 6;
            this.lbl_div2_div.Text = "------------------------------";
            // 
            // txt_div2_2
            // 
            this.txt_div2_2.Location = new System.Drawing.Point(175, 51);
            this.txt_div2_2.Name = "txt_div2_2";
            this.txt_div2_2.Size = new System.Drawing.Size(98, 20);
            this.txt_div2_2.TabIndex = 5;
            this.txt_div2_2.TextChanged += new System.EventHandler(this.collect_event);
            // 
            // txt_div2_1
            // 
            this.txt_div2_1.Location = new System.Drawing.Point(175, 12);
            this.txt_div2_1.Name = "txt_div2_1";
            this.txt_div2_1.Size = new System.Drawing.Size(98, 20);
            this.txt_div2_1.TabIndex = 4;
            this.txt_div2_1.TextChanged += new System.EventHandler(this.collect_event);
            // 
            // lbl_equals
            // 
            this.lbl_equals.AutoSize = true;
            this.lbl_equals.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_equals.Location = new System.Drawing.Point(279, 33);
            this.lbl_equals.Name = "lbl_equals";
            this.lbl_equals.Size = new System.Drawing.Size(18, 20);
            this.lbl_equals.TabIndex = 7;
            this.lbl_equals.Text = "=";
            // 
            // lbl_res_div
            // 
            this.lbl_res_div.AutoSize = true;
            this.lbl_res_div.Location = new System.Drawing.Point(312, 35);
            this.lbl_res_div.Name = "lbl_res_div";
            this.lbl_res_div.Size = new System.Drawing.Size(97, 13);
            this.lbl_res_div.TabIndex = 10;
            this.lbl_res_div.Text = "------------------------------";
            // 
            // txt_res_2
            // 
            this.txt_res_2.Location = new System.Drawing.Point(311, 51);
            this.txt_res_2.Name = "txt_res_2";
            this.txt_res_2.ReadOnly = true;
            this.txt_res_2.Size = new System.Drawing.Size(98, 20);
            this.txt_res_2.TabIndex = 9;
            // 
            // txt_res_1
            // 
            this.txt_res_1.Location = new System.Drawing.Point(311, 12);
            this.txt_res_1.Name = "txt_res_1";
            this.txt_res_1.ReadOnly = true;
            this.txt_res_1.Size = new System.Drawing.Size(98, 20);
            this.txt_res_1.TabIndex = 8;
            // 
            // frm_breuken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 322);
            this.Controls.Add(this.lbl_res_div);
            this.Controls.Add(this.txt_res_2);
            this.Controls.Add(this.txt_res_1);
            this.Controls.Add(this.lbl_equals);
            this.Controls.Add(this.lbl_div2_div);
            this.Controls.Add(this.txt_div2_2);
            this.Controls.Add(this.txt_div2_1);
            this.Controls.Add(this.txt_oper);
            this.Controls.Add(this.lbl_div1_div);
            this.Controls.Add(this.txt_div1_2);
            this.Controls.Add(this.txt_div1_1);
            this.Name = "frm_breuken";
            this.Text = "Breuken";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_div1_1;
        private System.Windows.Forms.TextBox txt_div1_2;
        private System.Windows.Forms.Label lbl_div1_div;
        private System.Windows.Forms.TextBox txt_oper;
        private System.Windows.Forms.Label lbl_div2_div;
        private System.Windows.Forms.TextBox txt_div2_2;
        private System.Windows.Forms.TextBox txt_div2_1;
        private System.Windows.Forms.Label lbl_equals;
        private System.Windows.Forms.Label lbl_res_div;
        private System.Windows.Forms.TextBox txt_res_2;
        private System.Windows.Forms.TextBox txt_res_1;
    }
}